+++
date = "2016-07-25T18:30:24-04:00"
title = "Objective"

+++
<html>
<body>
<h1><b>Background and Motivation</b></h1>

<p class="amjad">
DNA origami is the self assembly process of  DNA nanostructures using a long single stranded 
DNA (ssDNA) called the “scaffold” and multiple shorter ssDNA called “staples” in a one-pot thermal 
annealing reaction <b>[B.1]</b>. This method was first pioneered by Dr. Paul Rothemund, who, in 2006, 
successfully fabricated static 2D nanostructures with highly programmable geometries <b>[B.2]</b>. 
Since then, the design process has been streamlined with the development of CAD software <b>[B.3]</b>, 
and the design space of DNA origami has expanded to include static 3D structures with well-defined 
geometries <b>[B.4]</b>, void spaces <b>[B.5]</b>, and curvatures <b>[B.6]</b>, as well as dynamic structures capable 
of rotational and linear motion <b>[B.7]</b>. In addition, nanostructures have been utilized in 
molecular scale measurement <b>[B.8]</b>, in vitro and in vivo drug delivery devices <b>[B.9,B.10]</b> and 
have shown promise in cellular interaction and behavior interpretation <b>[B.10,B.11]</b>.</p>
<img src="https://gitlab.com/OhioMOD/OhioMOD/raw/5001f29cbd53551fc8e099a51e4ff5e671b30ef1/images/Background.png" title="Background">
<h3>Figure B.1. Biologically applications of DNA origami. <b>(A)</b> in vivo delivery of the 
chemotherapeutic doxorubicin into mouse tumors through intercalation into DNA origami structures. 
<b>(B)</b> DNA nanorobot that utilizes an aptamer recognition mechanism to release payload to target cells. 
<b>(C)</b> Virus inspired encapsulation of DNA origami octahedron by PEGylated lipid bilayer in order to 
improve stability and circulation time in the body. <b>(D)</b> Schematic of DNA origami based synthetic 
lipid membrane channels capable of controlling pore size. <b>(E)</b> DNA origami logic board for the 
detection of miRNA. Signal binding causes the release of a biotinylated strand that binds to 
the output section. Addition and binding of streptavidin to the biotinylated strand can be detected 
using AFM. <b>(F)</b> Schematic for DNA PAINT. The distance between two binding sites can be calculated by 
averaging the number transient binding and unbinding events of fluorescent strands to the binding sites.</h3>
<p class="amjad">
Even though the breadth of DNA origami has increased significantly in the last decade, several 
challenges remain in translating DNA origami design and fabrication principles for large 
scale manufacturing and practical applications such as in vivo drug delivery systems. Recently, 
Sobczak et al developed a rapid fabrication protocol for structure synthesis at a single 
temperature in hours, rather than the multi-day, multi-temperature reaction that was traditionally 
used <b>[B.12]</b>.  Furthermore, custom scaffold designs have been developed that reduce the amount of 
starting material required <b>[B.13]</b> and custom inkjet-printing can be used to fabricate inexpensive 
oligonucleotides <b>[B.14]</b>, further mitigating the cost of fabrication.</p>
<p class="amjad">
 In addition, the stability of these nanostructures in harsh conditions such as those in biological 
 systems is a concern. DNA origami is traditionally fabricated and stored in buffers containing 
 tens of mM Mg2+ in order to mitigate electrostatic repulsion between the negatively charged phosphate 
 backbone in DNA <b>[B.15]</b>. Martin et al showed that this limitation is mitigated by also successfully 
 fabricating DNA nanostructures at higher concentrations (hundreds of mM) of monovalent cations 
 such as Na+ <b>[B.16]</b>. However, most biological buffer systems have significantly lower concentrations 
 of Na+ and Mg2+ compared to DNA origami buffer conditions, posing one barrier to structure stability. 
 In addition, many biological mediums also contain nucleases that can enzymatically break down 
 the nanostructures before they can reach their destination <b>[B.17]</b>. Several groups have demonstrated 
 suboptimal stability and pharmacokinetics of DNA origami nanostructures in vivo in the absence 
 of additional modifications <b>[B.18]</b>. Furthermore, the stability of every new structure must 
 currently be determined experimentally, since no systematic study relating design parameters 
 to structure stability has been published.</p> 

<h1><b>Research Goals</b></h1>
<p class="amjad">DNA origami allows for the engineering of nanostructures with nanometer geometric 
precision. This self-assembly technique can contribute as both diagnostic and therapeutic means 
in nanomedicine, however, instability in biological environments remains an issue for in vivo 
applications. The main goal for OhioMOD 2016 is to perform stability experiments on a wide 
range of structures to correlate design factors of DNA Origami to nanostructure stability. 
Furthermore, we aim to design a structure where we implement the parameters that we determined 
from the experiments to improve stability.  The parameters we focused on include scaffold routing, 
staple length, staple segment length, structure geometry, surface area, and structure cross section 
in varying salt and serum concentrations. We hypothesize that each of these design characteristics 
impact the structure formation and denaturation in a unique manner allowing us to predict structure 
behavior. Furthermore, we intend to utilize our predictions in order to determine whether our new 
structure will perform as intended in certain environmental conditions such as high temperature, 
low salt, and/or biological serum.</p>  
<p class="amjad">Our future goals encompass testing the novel structure and verifying our predictions. 
Our long term goal is to compile all of our data into a predictive model database. Outside 
users would also be able to input a desired scaffold length, cross section and annealing temperature 
and our database will provide them with possible structures they could use as well as analyze 
stability of a newly designed structure before purchasing expensive oligonucleotides.</p>  
<img src="https://gitlab.com/OhioMOD/OhioMOD/raw/cce9b5440b4fd7f949dfa7f0c3635e25061f4057/images/SpecialRhincorn.gif" title="Dancing Rhinocorn">
<h3><b>Figure B.2: Dancing Rhinocorn</b></h3>
<h1><b>References</b></h1>
<ul class="ref">
<li>B.1: Castro, Carlos E et al. “A primer to scaffolded DNA origami”. Nature Methods 8, 221-229 (2011).</li>
<li>B.2: Rothemund Paul W.K. “Folding DNA to create nanoscale shapes and patterns”. Nature 440, 297-302 (2006).</li>
<li>B.3: Douglas, S.D., et al. Rapid prototyping of 3D DNA-origami shapes with caDNAno. Nucl. Acids Res. 44(18) (2009)</li>
<li>B.4: Douglas, Shawn M et al. “Self-assembly of DNA into nanoscale three-dimensional shapes”. Nature 459, 415-418 (2009)</li>
<li>B.5: Anderson, E.S. et al. Self-assembly of a nanoscale DNA box with a controllable lid. Nature 459 73-76 (2009)</li>
<li>B.6: Dietz, H., Douglas, S.M., & Shih, W.M. Folding DNA into twisted and curved nanoscale shapes. Science 325, 725-730 (2009).</li>
<li>B.7: Marras, A.E. et al. Programmable motion of DNA origami mechanisms. Proceedings of the National Academy of Sciences 112(3) 713-718 (2015).</li>
<li>B.8: Jungmann, R. et al. Multiplexed 3D cellular super-resolution imaging with DNA-PAINT and Exchange-PAINT. Nature Methods 11, 313-318 (2014).</li>
<li>B.9: Qiao, J. et al. DNA origami as a carrier for circumvention of drug resistance. Journal of the American Chemical Society. 124(32) 13396-13403 (2012).</li>
<li>B.10: Kearney, Cathal J. et al. "DNA Origami: Folded DNA‐Nanodevices That Can Direct and Interpret Cell Behavior." Advanced Materials (2016).</li>
<li>B.11 Modi, Souvik et al. "Two DNA nanomachines map pH changes along intersecting endocytic pathways inside the same cell." Nature nanotechnology 8.6 (2013): 459-467.</li>
<li>B.12: Sobczak, J. et al. Rapid folding of DNA into nanoscale shapes at constant temperature. Science 338 1458-1461 (2012).</li>
<li>B.13: Niekamp, S. et al. Folding complex DNA nanostructures from limited sets of reusable sequences. Nucleic Acids Research. 44(11) (2016).</li>
<li>B.14: Lausted, Christopher et al. "POSaM: a fast, flexible, open-source, inkjet oligonucleotide synthesizer and microarrayer." Genome biology 5.8 (2004).</li>
<li>B.15: Hud, Nicholas V., and Matjaz Polak. "DNA–cation interactions: the major and minor grooves are flexible ionophores." Current opinion in structural biology11.3 (2001): 293-301.</li>
<li>B.16: Martin, T.G, & Dietz, H. Magnesium-free self-assembly of multi-layer DNA objects. Nature Communications. 3 (2012).</li>
<li>B.17: Nadano, D., Yasuda, T., & Kishi, K. Measurement of Deoxyribonuclease I activity in human tissues and body fluids by a single radial enzyme-diffusion method. Clinical Chemistry. 39 448-452 (1993).</li>
<li>B.18: Perrault, S.D., & Shih, W.M. Virus-inspired membrane encapsulation of DNA nanostructures to achieve in vivo stability. Nano Letters. 8(5) 5132-5140 (2014).</li>
</ul>
</body>
  <div class="bottom">
  <footer>
  <ul class="bot">
  <li><a href="https://nbl.osu.edu/">NBL</a>
  theohiomod@gmail.com       
  <a href="https://www.facebook.com/OhioMod">Facebook</a>  
  <a href="http://biomod.net" class="rig">BioMOD</a></li>
  </footer>
  </div>
<style type="text/css">
.img{float:left;}
p.amjad{
font-family:"Helvetica";
font-size:100%;
text-align:justify; line-height:25px; margin-bottom:0.5cm;
color:black;}
h1{
font-family:"Helvetica";
font-size:100%; margin-bottom:1cm; text-align:center;}
ul.bot{margin-top: 4cm;
background-color:black; margin-bottom:-1cm; padding-bottom:-1px;
line-height:2em; position:relative; font-color: white;
margin-left:-20cm;; margin-right:-20cm; text-align:center;
font-family:'Press Start 2P', cursive; font-size:80%;
line-height:4em; word-spacing:6cm;}
ul.ref{
font-family:"Helvetica";
font-size:75%; color:black;}
h3{
font-size:80%; font-family:"Helvetica";
color:black; text-align:center;}
</style>
</html>